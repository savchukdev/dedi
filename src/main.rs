use derivative::Derivative;
use lazy_static::{__Deref, lazy_static};
use std::{
    collections::HashMap,
    error::Error,
    ffi::c_int,
    io::{self, stdin, stdout, Read, Write},
    mem::MaybeUninit,
    ops::Range,
    os::unix::prelude::AsRawFd,
    path::Path,
    sync::{Arc, RwLock},
    time::{Duration, Instant},
};

use dedi::{init_debug, log};

const VERSION: &str = "0.0.1";
const TAB_WIDTH: u32 = 4;
const ESCAPE: char = '\x1b';

const NORMAL_FG: usize = 39;
const NORMAL_BG: usize = 49;
const RED_FG: usize = 31;
const GREEN_FG: usize = 32;
const YELLOW_FG: usize = 33;
const MAGENTA_FG: usize = 35;
const CYAN_FG: usize = 36;
const WHITE_FG: usize = 37;
const _BLUE_FG: usize = 34;
const CYAN_BG: usize = 46;
const GREEN_BG: usize = 42;
const BLUE_BG: usize = 44;

struct Cleanup {
    term: libc::termios,
}

impl Drop for Cleanup {
    fn drop(&mut self) {
        unsafe {
            // not checking result here because it's supposed to run only once at the end of the
            // program. if the call wasn't successful - not much can do here. Will be praying
            // termios will get correctly reset by a terminal emulator
            let res = libc::tcsetattr(
                libc::TCSANOW,
                libc::TCSAFLUSH,
                &self.term as *const libc::termios,
            );
            assert_eq!(res, 0);
        };
    }
}

#[derive(Debug)]
enum InputAction {
    Exit,
    Char(char),
    MoveCursor(i32, i32),
    CursorTop,
    CursorBottom,
    CursorBeginning,
    CursorEnd,
    Enter,
    Delete,
    Backspace,
    Save,
    SaveAs,
    Search,
    Escape,
    None,
}

impl InputAction {
    pub fn cursor_up(steps: u32) -> InputAction {
        InputAction::MoveCursor(0, -(steps as i32))
    }

    pub fn cursor_down(steps: u32) -> InputAction {
        InputAction::MoveCursor(0, steps as i32)
    }

    pub fn cursor_left(steps: u32) -> InputAction {
        InputAction::MoveCursor(-(steps as i32), 0)
    }

    pub fn cursor_right(steps: u32) -> InputAction {
        InputAction::MoveCursor(steps as i32, 0)
    }
}

#[derive(Debug)]
struct Winsize {
    height: usize,
    width: usize,
}

#[derive(Derivative)]
#[derivative(Debug)]
struct Config<'a> {
    cx: i32,
    cy: i32,
    rx: i32,
    winsize: Winsize,
    rowoff: u32,
    coloff: u32,
    #[derivative(Debug = "ignore")]
    rows: Vec<Row>,
    filename: Option<String>,
    statusline: String,
    statusline_time: Instant,
    dirty: bool,
    syntax: Option<&'a Syntax>,
}

#[derive(Debug, Clone, Copy)]
struct Position {
    cy: i32,
    rx: i32,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Graphic {
    Normal,
    SearchHighlight,
    Number,
    String,
    Comment,
    Keyword,
    Keyword2,
}

impl Graphic {
    pub fn insert_color(&self, buf: &mut String) {
        match self {
            Graphic::Normal => {
                buf.push_str(&format!("\x1b[{}m", NORMAL_FG));
                buf.push_str(&format!("\x1b[{}m", NORMAL_BG));
            }
            Graphic::SearchHighlight => {
                buf.push_str(&format!("\x1b[{}m", WHITE_FG));
                buf.push_str(&format!("\x1b[{}m", BLUE_BG));
            }
            Graphic::Number => {
                buf.push_str(&format!("\x1b[{}m", RED_FG));
                buf.push_str(&format!("\x1b[{}m", NORMAL_BG));
            }
            Graphic::String => {
                buf.push_str(&format!("\x1b[{}m", GREEN_FG));
                buf.push_str(&format!("\x1b[{}m", NORMAL_BG));
            }
            Graphic::Comment => {
                buf.push_str(&format!("\x1b[{}m", CYAN_FG));
                buf.push_str(&format!("\x1b[{}m", NORMAL_BG));
            }
            Graphic::Keyword => {
                buf.push_str(&format!("\x1b[{}m", YELLOW_FG));
                buf.push_str(&format!("\x1b[{}m", NORMAL_BG));
            }
            Graphic::Keyword2 => {
                buf.push_str(&format!("\x1b[{}m", MAGENTA_FG));
                buf.push_str(&format!("\x1b[{}m", NORMAL_BG));
            }
        }
    }
}

#[derive(Debug, Clone)]
struct Syntax {
    // that's gonna be shown to the user as the current file type
    file_type: &'static str,
    // file extensions that match current type
    file_extensions: Vec<&'static str>,
    // bitmask that controls which syntax elements are processed(highlighted)
    syntax_flags: syntax_flags::SyntaxFlag,
    // single line comments start with
    single_line_comment: &'static str,
    multi_line_comment_start: &'static str,
    multi_line_comment_end: &'static str,
    // keywords map. Specified graphic will be applied to matched keyword
    keywords: &'static HashMap<&'static str, Graphic>,
}

impl Syntax {
    fn current() -> Arc<Option<Syntax>> {
        CURRENT_SYNTAX.with(|s| s.read().unwrap().clone())
    }

    fn make_current(syntax: Option<Self>) {
        CURRENT_SYNTAX.with(|s| *s.write().unwrap() = Arc::new(syntax))
    }
}

mod syntax_flags {
    pub type SyntaxFlag = usize;
    pub const NUMBERS: SyntaxFlag = 1 << 0;
    pub const STRINGS: SyntaxFlag = 1 << 1;
    pub const COMMENTS: SyntaxFlag = 1 << 2;
    pub const MULTILINE_COMMENTS: SyntaxFlag = 1 << 3;
    pub const KEYWORDS: SyntaxFlag = 1 << 4;
}

lazy_static! {
    static ref C_KEYWORDS: HashMap<&'static str, Graphic> = {
        let mut res = HashMap::new();

        [
            "#include", "switch", "if", "while", "for", "break", "continue", "return", "else",
            "struct", "union", "typedef", "static", "enum", "class", "case",
        ]
        .iter()
        .for_each(|&w| {
            res.insert(w, Graphic::Keyword);
        });

        [
            "int", "long", "double", "float", "char", "unsigned", "signed", "void",
        ]
        .iter()
        .for_each(|w| {
            res.insert(w, Graphic::Keyword2);
        });

        res
    };
    static ref HLDB: Vec<Syntax> = vec![Syntax {
        file_type: "c",
        file_extensions: vec!["c", "h"],
        syntax_flags: syntax_flags::NUMBERS
            | syntax_flags::STRINGS
            | syntax_flags::COMMENTS
            | syntax_flags::MULTILINE_COMMENTS
            | syntax_flags::KEYWORDS,
        single_line_comment: "//",
        multi_line_comment_start: "/*",
        multi_line_comment_end: "*/",
        keywords: C_KEYWORDS.deref(),
    }];
}

thread_local! {
    static CURRENT_SYNTAX: RwLock<Arc<Option<Syntax>>> = RwLock::new(Arc::new(None));
}

#[derive(Debug, Clone, Copy, Default)]
struct RowMetadata {
    hl_open_comment: bool,
}

#[derive(Debug)]
struct Row {
    data: String,
    render: String,
    graphic: Vec<Graphic>,
    hl_open_comment: bool,
}

enum PromptAction {
    Search,
}

fn is_separator(c: char) -> bool {
    c.is_whitespace() || c == '\0' || c.is_ascii_punctuation()
}

impl Row {
    pub fn new(data: String) -> Self {
        let render = Self::render_string(&data);

        let mut row = Row {
            data,
            render,
            graphic: vec![],
            hl_open_comment: false,
        };

        row.apply_syntax(RowMetadata::default());
        assert_eq!(row.render.len(), row.graphic.len());

        row
    }

    pub fn metadata(&self) -> RowMetadata {
        RowMetadata {
            hl_open_comment: self.hl_open_comment,
        }
    }

    pub fn re_render(&mut self, prev_row_info: RowMetadata) -> bool {
        self.render = Self::render_string(&self.data);

        let (g, c) = self.apply_graphics(prev_row_info);
        self.graphic = g;
        c
    }

    pub fn apply_syntax(&mut self, prev_row_info: RowMetadata) -> bool {
        let (g, c) = self.apply_graphics(prev_row_info);
        self.graphic = g;
        c
    }

    pub fn highlight(&mut self, r: Range<usize>, g: Graphic) {
        let mut graphics = vec![g].repeat(r.clone().count());
        self.graphic[r].swap_with_slice(&mut graphics[..])
    }

    fn apply_graphics(&mut self, prev_row_info: RowMetadata) -> (Vec<Graphic>, bool) {
        let mut graphic: Vec<Graphic> = vec![Graphic::Normal; self.render.len()];

        let syn = Syntax::current();
        let syntax = match *syn {
            Some(ref s) => s,
            None => return (graphic, self.hl_open_comment),
        };

        if syntax.syntax_flags == 0 {
            return (graphic, self.hl_open_comment);
        }

        let mut prev_is_separator = true;
        let mut in_string = '\0';
        let mut in_comment = prev_row_info.hl_open_comment;

        let mut idx = 0;
        while let Some(c) = self.render.chars().nth(idx) {
            let prev_graphic = match graphic.get(idx.saturating_sub(1)) {
                Some(g) => *g,
                None => Graphic::Normal,
            };

            if syntax.syntax_flags & syntax_flags::MULTILINE_COMMENTS != 0
                && !syntax.multi_line_comment_start.is_empty()
                && !syntax.multi_line_comment_end.is_empty()
            {
                if in_comment {
                    graphic[idx] = Graphic::Comment;
                    if self.render[idx..].starts_with(syntax.multi_line_comment_end) {
                        graphic[idx..idx + syntax.multi_line_comment_end.len()]
                            .iter_mut()
                            .for_each(|g| {
                                *g = Graphic::Comment;
                            });
                        in_comment = false;
                        prev_is_separator = false;
                        idx += syntax.multi_line_comment_end.len();
                        continue;
                    } else {
                        idx += 1;
                        continue;
                    }
                } else if self.render[idx..].starts_with(syntax.multi_line_comment_start) {
                    graphic[idx..idx + syntax.multi_line_comment_start.len()]
                        .iter_mut()
                        .for_each(|g| {
                            *g = Graphic::Comment;
                        });
                    in_comment = true;
                    idx += syntax.multi_line_comment_start.len();
                    continue;
                }
            }

            if syntax.syntax_flags & syntax_flags::COMMENTS != 0
                && !syntax.single_line_comment.is_empty()
                && self.render[idx..].starts_with(syntax.single_line_comment)
            {
                graphic[idx..].iter_mut().for_each(|g| {
                    *g = Graphic::Comment;
                });
                break;
            }

            if syntax.syntax_flags & syntax_flags::STRINGS != 0 {
                if in_string != '\0' {
                    graphic[idx] = Graphic::String;
                    if c == '\\' {
                        graphic[idx + 1] = Graphic::String;
                        idx += 2;
                        continue;
                    }
                    if c == in_string {
                        in_string = '\0';
                    }
                    prev_is_separator = true;
                    idx += 1;
                    continue;
                } else if c == '\'' || c == '"' {
                    in_string = c;
                    graphic[idx] = Graphic::String;
                    idx += 1;
                    continue;
                }
            }

            // if current syntax has numbers enabled and
            // (current char is a digit and previous one was a separator or a digit) or
            // (current char is a dot and previous one was a digit)
            if syntax.syntax_flags & syntax_flags::NUMBERS != 0
                && (c.is_ascii_digit() && (prev_is_separator || prev_graphic == Graphic::Number))
                || (c == '.' && prev_graphic == Graphic::Number)
            {
                graphic[idx] = Graphic::Number;
                prev_is_separator = false;
                idx += 1;
                continue;
            }

            if prev_is_separator {
                let keyword = syntax.keywords.iter().find(|(&keyword, &new_g)| {
                    let ends_with_separator =
                        is_separator(self.render.chars().nth(idx + keyword.len()).unwrap_or('\0'));

                    if self.render[idx..].starts_with(keyword) && ends_with_separator {
                        graphic[idx..idx + keyword.len()].iter_mut().for_each(|g| {
                            *g = new_g;
                        });

                        idx += keyword.len();

                        true
                    } else {
                        false
                    }
                });

                if keyword.is_some() {
                    prev_is_separator = false;
                    continue;
                }
            }

            prev_is_separator = is_separator(c);
            idx += 1;
        }

        let changed = self.hl_open_comment != in_comment;
        self.hl_open_comment = in_comment;

        (graphic, changed)
    }

    fn render_string(s: &str) -> String {
        let mut render = String::with_capacity(s.len());
        for c in s.chars() {
            match c {
                '\t' => {
                    for _ in 0..TAB_WIDTH {
                        render.push(' ');
                    }
                }
                _ => render.push(c),
            }
        }

        render
    }
}

impl<'a> Config<'a> {
    pub fn new(ws: Winsize) -> Self {
        Config {
            cx: 0,
            cy: 0,
            rx: 0,
            rows: Vec::with_capacity(ws.height),
            statusline_time: Instant::now(),
            statusline: String::from(""),
            rowoff: 0,
            coloff: 0,
            filename: None,
            winsize: ws,
            dirty: false,
            syntax: None,
        }
    }

    pub fn open_file(&mut self, filename: Option<&str>) -> io::Result<()> {
        let (filename, rows) = match filename {
            Some(name) => {
                let mut file = std::fs::OpenOptions::new()
                    .read(true)
                    .create(true)
                    .write(true)
                    .open(name)?;
                let metadata = match file.metadata() {
                    Ok(md) => md,
                    Err(e) => return Err(e),
                };
                let mut buf: Vec<u8> = Vec::with_capacity(metadata.len() as usize);
                file.read_to_end(&mut buf)?;

                let text = String::from_utf8_lossy(&buf).into_owned();
                let rows: Vec<String> = text.lines().map(String::from).collect();

                (Some(name), rows)
            }
            None => (None, vec![]),
        };

        self.filename = filename.map(String::from);
        self.set_file_syntax();

        let mut row_meta = RowMetadata::default();
        self.rows = rows
            .into_iter()
            .map(|r| {
                let mut r = Row::new(r);
                r.apply_syntax(row_meta);
                row_meta = r.metadata();
                r
            })
            .collect();

        self.cy = 0;
        self.cx = 0;
        self.rx = 0;
        self.rowoff = 0;
        self.coloff = 0;
        self.dirty = false;

        Ok(())
    }

    pub fn update(&mut self) {
        self.fix_offsets();
        self.fix_cursor_position();
        self.recalc_rx();
        self.recalc_syntax();
    }

    pub fn set_status(&mut self, message: &str) {
        self.statusline = message.to_string();
        self.statusline_time = Instant::now();
    }

    pub fn set_file_syntax(&mut self) {
        let name = match self.filename.as_ref() {
            Some(name) => name,
            None => return,
        };

        let path = Path::new(&name);
        let extension = path.extension().map(|e| e.to_str().unwrap_or(""));

        match extension {
            Some(ext) => {
                if let Some(s) = HLDB.iter().find(|s| s.file_extensions.contains(&ext)) {
                    Syntax::make_current(Some(s.clone()))
                }
            }
            None => Syntax::make_current(None),
        }
    }

    pub fn prompt(&mut self, prompt: &str, prompt_action: Option<PromptAction>) -> Option<String> {
        let mut input = String::with_capacity(self.winsize.width);

        #[derive(Debug)]
        struct Search {
            matches: Vec<Position>,
            orig_hl: Vec<(usize, Vec<Graphic>)>,
            current: usize,
        }

        let mut search_results = Search {
            matches: vec![],
            orig_hl: vec![],
            current: 0,
        };

        loop {
            self.set_status(&format!("{} {}", prompt, &input));
            refresh_screen(self).unwrap_or(());

            let action = process_input().unwrap_or(InputAction::None);
            match action {
                InputAction::Char(c) => {
                    input.push(c);
                    match prompt_action {
                        Some(PromptAction::Search) => {
                            search_results.matches = self.search(&input);
                            search_results.current = 0;

                            for (row, g) in search_results.orig_hl.drain(..) {
                                if let Some(row) = self.rows.get_mut(row) {
                                    let _ = std::mem::replace(&mut row.graphic, g);
                                }
                            }

                            let mut current_row = -1;
                            for &Position { cy, rx } in &search_results.matches {
                                if current_row != cy {
                                    current_row = cy;
                                    if let Some(row) = self.rows.get_mut(cy as usize) {
                                        search_results
                                            .orig_hl
                                            .push((cy as usize, row.graphic.clone()));
                                    }
                                }

                                if let Some(row) = self.rows.get_mut(cy as usize) {
                                    row.highlight(
                                        rx as usize..rx as usize + input.len(),
                                        Graphic::SearchHighlight,
                                    )
                                }
                            }

                            if let Some(pos) = search_results.matches.get(0) {
                                self.jump_to(*pos)
                            }
                        }
                        None => {}
                    }
                }
                InputAction::Escape => {
                    for (row, g) in search_results.orig_hl.drain(..) {
                        if let Some(row) = self.rows.get_mut(row) {
                            let _ = std::mem::replace(&mut row.graphic, g);
                        }
                    }
                    self.set_status("");
                    return None;
                }
                // up
                InputAction::MoveCursor(0, -1) => {
                    search_results.current = search_results.current.saturating_sub(1);
                    if let Some(pos) = search_results.matches.get(search_results.current) {
                        self.jump_to(*pos)
                    }
                }
                // down
                InputAction::MoveCursor(0, 1) => {
                    search_results.current = search_results
                        .current
                        .saturating_add(1)
                        .min(search_results.matches.len());
                    if let Some(pos) = search_results.matches.get(search_results.current) {
                        self.jump_to(*pos)
                    }
                }
                InputAction::Enter => {
                    for (row, g) in search_results.orig_hl.drain(..) {
                        if let Some(row) = self.rows.get_mut(row) {
                            let _ = std::mem::replace(&mut row.graphic, g);
                        }
                    }
                    self.set_status("");
                    if input.is_empty() {
                        return None;
                    } else {
                        return Some(input);
                    }
                }
                InputAction::Delete | InputAction::Backspace => {
                    input.pop();
                }
                _ => {}
            }
        }
    }

    pub fn search(&mut self, query: &str) -> Vec<Position> {
        self.rows
            .iter()
            .enumerate()
            .filter_map(|(n, row)| {
                let indecies: Vec<(usize, &str)> = row.render.match_indices(query).collect();
                if indecies.is_empty() {
                    None
                } else {
                    let positions: Vec<Position> = indecies
                        .iter()
                        .map(|(pos, _)| Position {
                            cy: n as i32,
                            rx: *pos as i32,
                        })
                        .collect();
                    Some(positions)
                }
            })
            .flatten()
            .collect()
    }

    pub fn jump_to(&mut self, pos: Position) {
        self.cy = pos.cy;
        self.rx = pos.rx;
        self.recalc_x();
        self.update();
    }

    pub fn remove_char_before(&mut self) {
        let col_i = self.cx - 1;
        self.cx -= 1;
        self.remove_char_at(col_i);
    }

    pub fn remove_char(&mut self) {
        let col_i = self.cx.max(0);
        self.remove_char_at(col_i);
    }

    fn remove_char_at(&mut self, idx: i32) {
        let current_row = match self.rows.get(self.cy as usize) {
            Some(row) => row,
            None => return,
        };

        if idx < 0 {
            if self.cy == 0 {
                return;
            }
            let prev_line_len = match self.rows.get((self.cy - 1) as usize) {
                Some(row) => row.data.len(),
                None => 0,
            };
            self.join_lines(self.cy - 1);
            self.cy -= 1;
            self.cx = prev_line_len as i32;
        } else if idx as usize >= current_row.data.len() {
            if self.cy as usize == self.rows.len() {
                return;
            }
            self.join_lines(self.cy);
        } else {
            let row = match self.rows.get_mut(self.cy as usize) {
                Some(row) => row,
                None => return,
            };

            let remove_index = row
                .data
                .char_indices()
                .nth(idx as usize)
                .map(|(pos, _)| pos)
                .unwrap_or(row.data.len());

            row.data.remove(remove_index);

            self.dirty = true;
            row.re_render(RowMetadata::default());
        }

        self.update();
    }

    fn recalc_syntax(&mut self) {
        let start_row = (self.cy - 2).max(0) as usize;
        let mut row_meta = match self.rows.get(start_row) {
            Some(r) => r.metadata(),
            None => RowMetadata::default(),
        };

        let mut row_idx = start_row as usize;
        let mut change_attempts = 3;

        while let Some(row) = self.rows.get_mut(row_idx) {
            let changed = row.apply_syntax(row_meta);
            row_meta = row.metadata();
            row_idx += 1;

            if !changed {
                change_attempts -= 1;
                if change_attempts == 0 {
                    return;
                }
            } else {
                change_attempts += 1;
            }
        }
    }

    // row specifies the first line.
    // line with index `row + 1` will be appended to the line with index `row`,
    // then `row + 1` will be removed
    fn join_lines(&mut self, row: i32) {
        if row < 0 || (row + 1) as usize > self.rows.len() {
            return;
        }
        let (r1, r2) = self.rows.split_at_mut((row + 1) as usize);

        // make sure both lines exist
        let first_row = match r1.last_mut() {
            Some(line) => line,
            None => return,
        };

        let second_row = match r2.first_mut() {
            Some(line) => line,
            None => return,
        };

        first_row.data.push_str(&second_row.data);
        first_row.re_render(RowMetadata {
            hl_open_comment: false,
        });

        self.rows.remove((row + 1) as usize);
    }

    pub fn insert_newline(&mut self) {
        self.split_line(self.cy);
        self.cy += 1;
        self.cx = 0;

        self.update();
    }

    fn split_line(&mut self, row: i32) {
        if row < 0 || row as usize >= self.rows.len() {
            return;
        }

        self.rows
            .insert((row + 1).max(0) as usize, Row::new(String::new()));

        let (r1, r2) = self.rows.split_at_mut((row + 1) as usize);

        // make sure both lines exist
        let current_row = match r1.last_mut() {
            Some(line) => line,
            None => return,
        };

        let new_row = match r2.first_mut() {
            Some(line) => line,
            None => return,
        };

        let current_pos = current_row
            .data
            .char_indices()
            .nth(self.cx as usize)
            .map(|(pos, _)| pos)
            .unwrap_or(current_row.data.len());

        new_row.data.push_str(&current_row.data[current_pos..]);
        current_row.data.replace_range(current_pos.., "");

        current_row.re_render(RowMetadata {
            hl_open_comment: false,
        });
        new_row.re_render(RowMetadata {
            hl_open_comment: false,
        });
    }

    pub fn insert_char(&mut self, c: char) {
        let row = match self.rows.get_mut(self.cy as usize) {
            Some(row) => row,
            None => {
                let row = Row::new(String::new());
                self.rows.insert(self.cy as usize, row);
                match self.rows.get_mut(self.cy as usize) {
                    Some(row) => row,
                    None => return,
                }
            }
        };

        let col_i = self.cx as usize;

        let col = if col_i >= row.data.len() {
            row.data.len()
        } else {
            col_i
        };

        row.data.insert(
            row.data
                .char_indices()
                .nth(col)
                .map(|(pos, _)| pos)
                .unwrap_or(row.data.len()),
            c,
        );

        self.dirty = true;
        row.re_render(RowMetadata {
            hl_open_comment: false,
        });
        self.cx += 1;
        self.update();
    }

    pub fn save_to_file(&mut self) {
        let filename = match self.filename.as_ref() {
            Some(name) => name,
            None => return,
        };

        let content: Vec<&str> = self.rows.iter().map(|row| row.data.as_str()).collect();
        let text = content.join("\n");
        let bytes = text.as_bytes();

        let mut file = match std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(filename)
        {
            Ok(file) => file,
            Err(err) => {
                self.set_status(&format!("Can't save: {}", err));
                return;
            }
        };
        match file.write_all(bytes) {
            Ok(_) => {
                self.set_status(&format!("Written {} bytes to {}", bytes.len(), filename));
                self.dirty = false;
            }
            Err(err) => {
                self.set_status(&err.to_string());
            }
        }
    }

    pub fn save_to_file_as(&mut self, new_name: &str) {
        let is_new_filename = match self.filename.as_ref() {
            Some(name) => name.ne(new_name),
            None => true,
        };

        self.filename = Some(new_name.to_string());
        self.save_to_file();
        self.set_file_syntax();

        let mut row_meta = RowMetadata::default();

        if is_new_filename {
            self.rows.iter_mut().for_each(|r| {
                r.apply_syntax(row_meta);
                row_meta = r.metadata();
            });
        }
    }

    fn recalc_rx(&mut self) {
        self.rx = match self.rows.get(self.cy as usize) {
            Some(row) => row
                .data
                .chars()
                .take(self.cx as usize)
                .map(|c| match c {
                    '\t' => TAB_WIDTH as i32,
                    _ => 1,
                })
                .sum(),
            None => 0,
        }
    }

    fn recalc_x(&mut self) {
        self.cx = match self.rows.get(self.cy as usize) {
            Some(row) => {
                let mut rx = 0;
                row.data
                    .chars()
                    .take_while(|c| {
                        rx += match c {
                            '\t' => TAB_WIDTH as i32,
                            _ => 1,
                        };

                        rx <= self.rx
                    })
                    .count() as i32
            }
            None => 0,
        }
    }

    fn fix_cursor_position(&mut self) {
        self.cy = self.cy.min(self.rows.len() as i32 - 1).max(0);
        self.cx = self
            .cx
            .min(
                self.rows
                    .get(self.cy as usize)
                    .map_or(0, |l| l.render.len()) as i32,
            )
            .max(0);

        if self.cy < self.rowoff as i32 {
            self.rowoff = self.cy as u32;
        }
        if self.cx < self.coloff as i32 {
            self.coloff = self.cx as u32;
        }
    }

    fn fix_offsets(&mut self) {
        if self.cy < self.rowoff as i32 {
            self.rowoff = self.cy.max(0) as u32;
        } else if self.cy >= (self.rowoff as i32 + self.winsize.height as i32) as i32 {
            self.rowoff = (self.cy - self.winsize.height as i32 + 1)
                .min((self.rows.len() as i32 - self.winsize.height as i32).max(0))
                as u32;
        }

        if self.cx < self.coloff as i32 {
            self.coloff = self.cx as u32;
        } else if self.cx >= (self.coloff + (self.winsize.width as u32)) as i32 {
            self.coloff = (self.cx as u32 - self.winsize.width as u32) + 1;
        }
        let current_row_len = match self.rows.get((self.cy).max(0) as usize) {
            Some(row) => row.data.len(),
            None => 0,
        };
        self.coloff = self.coloff.min(
            ((self.cx + current_row_len as i32 - self.winsize.width as i32).max(0) as usize) as u32,
        );
    }
}

fn main() {
    match run() {
        Ok(_) => {}
        Err(e) => eprintln!("Error: {}", e),
    }
}

fn run() -> Result<(), Box<dyn Error>> {
    init_debug("log")?;

    let termios: MaybeUninit<libc::termios> = MaybeUninit::uninit();
    let res = unsafe { libc::tcgetattr(libc::STDIN_FILENO, termios.as_ptr() as *mut _) };

    if res != 0 {
        return Err("can't get termios info".into());
    }

    let mut termios = unsafe { termios.assume_init() };
    let _cleanup = Cleanup { term: termios };

    termios.c_lflag &= !libc::ECHO; // disable echoing in terminal
    termios.c_lflag &= !libc::ICANON; // disable canonical mode
    termios.c_lflag &= !libc::ISIG; // disable signals (ctrl-c & ctrl-z)
    termios.c_lflag &= !libc::IEXTEN; // disable ctrl-v
                                      //
    termios.c_iflag &= !libc::ICRNL; // disable carriage return and new line (ctrl-m)
    termios.c_iflag &= !libc::IXON; // disable XON/XOFF flow control on output (ctrl-s & ctrl-q)

    termios.c_oflag &= !libc::OPOST; // disable output processing
    termios.c_oflag |= libc::CS8; // set character size to 8 bits

    termios.c_cc[libc::VMIN] = 0; // min bytes to read to `read` before returning
    termios.c_cc[libc::VTIME] = 1; // min time to wait for `read` before returning. In tenths of seconds.

    std_write("\x1b[?1049h").ok();

    // TCSAFLUSH flushes all pending output and discards input
    let res = unsafe {
        libc::tcsetattr(
            libc::STDIN_FILENO,
            libc::TCSAFLUSH,
            &termios as *const libc::termios,
        )
    };
    if res != 0 {
        return Err("can't set raw mode".into());
    }

    let mut winsize = match get_winsize() {
        Some(ws) => ws,
        None => return Err("can't get current windows size".into()),
    };
    winsize.height -= 2; // one for status and another for the prompt

    let mut config = Config::new(winsize);
    config.open_file(std::env::args().nth(1).as_deref())?;

    log!("new run");

    config.set_status("HELP: Ctrl-Q = quit");

    let mut need_refresh = true;
    let mut quit_attempt = false;
    loop {
        if need_refresh {
            log!("refresh");
            refresh_screen(&config)?;
        }
        need_refresh = true;

        let action = process_input()?;
        match action {
            InputAction::Exit => {
                if config.dirty && !quit_attempt {
                    config.set_status("Unsaved changed! Press Ctrl-Q again to force quit.");
                    quit_attempt = true;
                    continue;
                }
                std_write("\x1b[2J").map_err(|_| "Can't clean up")?;
                std_write("\x1b[H").map_err(|_| "Can't clean up")?;
                break;
            }
            InputAction::MoveCursor(x, y) => {
                config.cx = 0.max(config.cx + x);
                config.cy = 0.max(config.cy + y);
            }
            InputAction::CursorTop => {
                config.cy -= config.winsize.height as i32 - 1;
            }
            InputAction::CursorBottom => {
                config.cy += config.winsize.height as i32 - 1;
            }
            InputAction::CursorBeginning => {
                config.cx = 0;
            }
            InputAction::CursorEnd => {
                let line_length = match config.rows.get(config.cy as usize) {
                    Some(row) => row.render.len(),
                    None => 0,
                };

                config.cx = line_length.max(config.winsize.width - 1) as i32;
            }
            InputAction::None => {
                need_refresh = false;
                continue;
            }
            InputAction::Delete => {
                config.remove_char();
            }
            InputAction::Backspace => {
                config.remove_char_before();
            }
            InputAction::Enter => {
                config.insert_newline();
            }
            InputAction::Save => {
                config.save_to_file();
            }
            InputAction::SaveAs => match config.prompt("Save as:", None) {
                Some(filename) => config.save_to_file_as(&filename),
                None => config.set_status("Save aborted"),
            },
            InputAction::Search => {
                let cx = config.cx;
                let rx = config.rx;
                let cy = config.cy;
                let rowoff = config.rowoff;
                let coloff = config.coloff;

                match config.prompt("Search: ", Some(PromptAction::Search)) {
                    Some(_) => {}
                    None => {
                        config.cx = cx;
                        config.rx = rx;
                        config.cy = cy;
                        config.rowoff = rowoff;
                        config.coloff = coloff;
                    }
                }
            }
            InputAction::Char(c) => {
                config.insert_char(c);
                need_refresh = true;
            }
            InputAction::Escape => {}
        }

        config.update();
        quit_attempt = false;
    }

    // refresh_screen(&config)?;

    Ok(())
}

fn process_input() -> Result<InputAction, Box<dyn Error>> {
    let input = read_input()?;

    match input {
        Input::Control(control) => {
            let action = match control {
                Control::Left => InputAction::cursor_left(1),
                Control::Right => InputAction::cursor_right(1),
                Control::Up => InputAction::cursor_up(1),
                Control::Down => InputAction::cursor_down(1),
                Control::PageUp => InputAction::CursorTop,
                Control::PageDown => InputAction::CursorBottom,
                Control::Home => InputAction::CursorBeginning,
                Control::End => InputAction::CursorEnd,
                Control::Delete => InputAction::Delete,
                Control::Alt(c) => match c {
                    's' => InputAction::SaveAs,
                    _ => InputAction::None,
                },
            };

            Ok(action)
        }
        Input::Char(c) => {
            if c == with_ctrl('q') {
                return Ok(InputAction::Exit);
            }

            if c == with_ctrl('l') {
                return Ok(InputAction::None);
            }

            if c == with_ctrl('s') {
                return Ok(InputAction::Save);
            }

            if c == with_ctrl('f') {
                return Ok(InputAction::Search);
            }

            if c == ESCAPE {
                return Ok(InputAction::Escape);
            }

            match c {
                '\0' | '\x1b' => Ok(InputAction::None),
                '\u{7f}' | '\u{8}' => Ok(InputAction::Backspace),
                '\r' | '\n' => Ok(InputAction::Enter),
                '\t' => Ok(InputAction::Char(c)),
                _ => {
                    if !c.is_control() {
                        Ok(InputAction::Char(c))
                    } else {
                        Ok(InputAction::None)
                    }
                }
            }
        }
    }
}

#[derive(Debug)]
enum Control {
    Up,
    Down,
    Left,
    Right,
    PageUp,
    PageDown,
    Home,
    End,
    Delete,
    Alt(char),
}

#[derive(Debug)]
enum Input {
    Control(Control),
    Char(char),
}

fn read_input() -> Result<Input, Box<dyn Error>> {
    let c = read_char()?;

    if c == ESCAPE {
        let c0 = match read_char() {
            Err(_) => return Ok(Input::Char(ESCAPE)),
            Ok(c) => c,
        };

        let c1 = match read_char() {
            Err(_) => return Ok(Input::Char(ESCAPE)),
            Ok(c) => c,
        };

        if c0 == '[' {
            match c1 {
                '0'..='9' => {
                    let c2 = match read_char() {
                        Err(_) => return Ok(Input::Char(ESCAPE)),
                        Ok(c) => c,
                    };

                    if c2 == '~' {
                        match c1 {
                            '1' => return Ok(Input::Control(Control::Home)),
                            '3' => return Ok(Input::Control(Control::Delete)),
                            '4' => return Ok(Input::Control(Control::End)),
                            '5' => return Ok(Input::Control(Control::PageUp)),
                            '6' => return Ok(Input::Control(Control::PageDown)),
                            '7' => return Ok(Input::Control(Control::Home)),
                            '8' => return Ok(Input::Control(Control::End)),
                            _ => {}
                        }
                    }
                }
                'A' => return Ok(Input::Control(Control::Up)),
                'B' => return Ok(Input::Control(Control::Down)),
                'C' => return Ok(Input::Control(Control::Right)),
                'D' => return Ok(Input::Control(Control::Left)),
                'H' => return Ok(Input::Control(Control::Home)),
                'F' => return Ok(Input::Control(Control::End)),
                _ => {}
            }

            return Ok(Input::Char(ESCAPE));
        } else if c0 == 'O' {
            match c1 {
                'H' => return Ok(Input::Control(Control::Home)),
                'F' => return Ok(Input::Control(Control::End)),
                _ => {}
            }
            return Ok(Input::Char(ESCAPE));
        } else if c1 == '\0' && c0 != '\0' {
            return Ok(Input::Control(Control::Alt(c0)));
        }
    }

    Ok(Input::Char(c))
}

/// reads a single byte from stdin
fn read_char() -> Result<char, Box<dyn Error>> {
    let c: char = '\0';
    let read = unsafe { libc::read(stdin().as_raw_fd(), &c as *const _ as *mut _, 1) };

    if read == -1 && errno() != libc::EAGAIN {
        return Err("can't read input".into());
    }

    Ok(c)
}

fn errno() -> c_int {
    unsafe { *libc::__errno_location() }
}

fn refresh_screen(config: &Config) -> Result<(), Box<dyn Error>> {
    let mut buf = String::with_capacity(config.winsize.height * config.winsize.width);

    buf.push_str("\x1b[?25l"); // hide cursor
    buf.push_str("\x1b[H"); // reset cursor position

    draw_rows(config, &mut buf);
    draw_status_bar(config, &mut buf);
    draw_message(config, &mut buf);

    buf.push_str("\x1b[H"); // reset cursor position
    buf.push_str(&format!(
        "\x1b[{};{}H",
        (config.cy - config.rowoff as i32) + 1,
        (config.rx - config.coloff as i32) + 1
    )); // set cursor position

    buf.push_str("\x1b[?25h"); // show cursor

    std_write(&buf).map_err(|_| "can't refresh screen appropriately")?;

    Ok(())
}

fn std_write(s: &str) -> Result<(), ()> {
    let written: usize =
        unsafe { libc::write(stdout().as_raw_fd(), s as *const _ as *mut _, s.len()) as usize };

    if written != s.len() {
        return Err(());
    }

    Ok(())
}

fn draw_rows(config: &Config, buf: &mut String) {
    for i in 0..config.winsize.height {
        match config.rows.get(config.rowoff as usize + i) {
            Some(row) => {
                let len = config
                    .winsize
                    .width
                    .min(row.render.len().saturating_sub(config.coloff as usize))
                    .max(0);

                let mut current_graphic = Graphic::Normal;
                for (c1, &graphic) in row
                    .render
                    .chars()
                    .zip(row.graphic.iter())
                    .skip(config.coloff as usize)
                    .take(len)
                {
                    if c1 == char::REPLACEMENT_CHARACTER {
                        buf.push_str("\x1b[7m");
                        buf.push(c1);
                        buf.push_str("\x1b[m");
                        current_graphic.insert_color(buf);
                    } else {
                        if graphic != current_graphic {
                            current_graphic = graphic;
                            current_graphic.insert_color(buf);
                        }
                        buf.push(c1);
                    }
                }

                buf.push_str(&format!("\x1b[{}m", NORMAL_FG));
                buf.push_str(&format!("\x1b[{}m", NORMAL_BG));

                buf.push_str("\x1b[K");
                buf.push_str("\r\n");
            }
            None => break,
        }
    }

    if config.rows.len() < config.winsize.height {
        for i in config.rows.len()..config.winsize.height - 1 {
            if i == config.winsize.height / 3 && config.rows.is_empty() {
                let message = format!("Duper editor --- version {}", VERSION);
                let padding = (config.winsize.width - message.len()) / 2;

                buf.push_str(&" ".repeat(padding));
                buf.push_str(&message);
            } else {
                buf.push('~');
            }

            buf.push_str("\x1b[K");
            buf.push_str("\r\n");
        }
        buf.push('~');
        buf.push_str("\x1b[K");
        buf.push_str("\r\n");
    }
}

fn draw_status_bar(config: &Config, buf: &mut String) {
    buf.push_str("\x1b[7m"); // invert colors

    let total_width = config.winsize.width;

    let rstatus = match config.syntax {
        Some(syntax) => format!(
            "{} | {}:{}",
            syntax.file_type,
            config.rows.len(),
            config.cy + 1
        ),
        None => format!("{}:{}", config.rows.len(), config.cy + 1),
    };
    let modified = if config.dirty { "(modified) " } else { "" };
    let lines = format!(" - {} lines {}", config.rows.len(), modified);

    let mut filename = match config.filename.as_ref() {
        Some(name) => name.clone(),
        None => "[No name]".to_string(),
    };

    if rstatus.len() + lines.len() + filename.len() >= total_width {
        filename = format!(
            "{}...",
            &filename[..(total_width - rstatus.len() - lines.len() - 3)]
        );
    }

    buf.push_str(&filename);
    buf.push_str(&lines);
    buf.push_str(&" ".repeat(total_width - filename.len() - rstatus.len() - lines.len()));
    buf.push_str(&rstatus);

    buf.push_str("\x1b[m"); // invert them back
    buf.push_str("\r\n");
}

fn draw_message(config: &Config, buf: &mut String) {
    buf.push_str("\x1b[K");
    if config.statusline_time.elapsed() < Duration::from_secs(5) {
        buf.push_str(&config.statusline);
    }
}

/// Returns either current winsize of None in case it's unsupported.
fn get_winsize() -> Option<Winsize> {
    let ws: MaybeUninit<libc::winsize> = MaybeUninit::uninit();
    let res = unsafe { libc::ioctl(stdin().as_raw_fd(), libc::TIOCGWINSZ, &ws as *const _) };

    if (res) == -1 {
        return get_winsize_vt100way();
    }

    let ws = unsafe { ws.assume_init() };

    Some(Winsize {
        height: ws.ws_row as usize,
        width: ws.ws_col as usize,
    })
}

/// Returns either current winsize of None in case it's unsupported.
/// Only use this if ioctl call fails.
fn get_winsize_vt100way() -> Option<Winsize> {
    // try moving cursor forward 999 chars and down 999 lines
    if std_write("\x1b[999C\x1b[999B").is_err() {
        return None;
    }

    // report device status (current position in this case)
    if std_write("\x1b[6n").is_err() {
        return None;
    }

    let mut s = String::with_capacity(32);
    if stdin().read_line(&mut s).is_err() {
        return None;
    }

    if s.len() < 4 {
        return None;
    }

    let mut bytes = s.bytes();
    if bytes.next().ne(&Some(ESCAPE as u8)) || bytes.next().ne(&Some(b'[')) {
        return None;
    }

    // status line ends with R. Remove it
    s.pop();
    // remove first two bytes also
    s.replace_range(..2, "");

    let mut it = s.splitn(2, ';');
    let height = it.next().map(|h| h.parse::<usize>())?.ok()?;
    let width = it.next().map(|h| h.parse::<usize>())?.ok()?;

    Some(Winsize { height, width })
}

fn with_ctrl(c: char) -> char {
    let int: u8 = c as u8;
    (int & 0x1f) as char
}
