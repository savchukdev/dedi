use std::fs::{File, OpenOptions};

static mut LOG: Logger = Logger { file: None };

struct Logger {
    file: Option<File>,
}

#[cfg(debug_assertions)]
pub fn into_file(data: String) {
    use std::io::Write;
    match unsafe { LOG.file.as_mut() } {
        Some(file) => {
            writeln!(file, "{}", data).unwrap();
        }
        None => {
            eprintln!("can't write");
        }
    }
}

#[cfg(not(debug_assertions))]
pub fn into_file(_: String) {}

#[cfg(debug_assertions)]
pub fn init_debug(filename: &str) -> Result<(), std::io::Error> {
    let file = OpenOptions::new()
        .append(true)
        .create(true)
        .open(filename)?;

    unsafe { LOG.file = Some(file) };

    Ok(())
}

#[cfg(not(debug_assertions))]
pub fn init_debug(_: &str) -> Result<(), std::io::Error> {
    Ok(())
}

#[macro_export]
macro_rules! log {
    ($val:expr) => {
        match $val {
            tmp => {
                $crate::into_file(format!(
                    "{}:{:?} ({}): {:#?}",
                    file!(), line!(),
                    stringify!($val),
                    &tmp
                ));

                tmp
            }
        }
    };
    ($($val:expr),+ $(,)?) => {
        ($($crate::log!($val)),+,)
    }
}
