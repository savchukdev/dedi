Simple terminal editor. Built on the ideas of the [kilo](https://viewsourcecode.org/snaptoken/kilo) editor.
